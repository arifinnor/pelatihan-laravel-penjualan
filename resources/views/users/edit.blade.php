@extends('layouts.main')

@section('title', 'Kategori Produk')

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('category.index') }}">Daftar Kategori</a></li>
<li class="breadcrumb-item active">Edit Kategori</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header">Edit Kategori</div>
    <div class="card-body">
        <div class="container">
            <form action="{{  route('user.update', [$user->id]) }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Nama Lengkap</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                        id="name" name="name" value="{{ old('name') }}"
                        value="{{ $user->name }}" placeholder="Nama Lengkap">
                        @error('name')
                            <label for="name" class="invalid-feedback">{{ $message }}</label>
                        @enderror
                </div>
                <div class="form-group">
                    <label for="email">Alamat Email</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror"
                        id="email" name="email" value="{{ old('email') }}"
                        value="{{ $user->email }}" placeholder="Alamat lengkap">
                        @error('email')
                            <label for="email" class="invalid-feedback">{{ $message }}</label>
                        @enderror
                </div>
                <div class="form-group">
                    <label for="phone">No. Telepon</label>
                    <input type="phone" class="form-control @error('phone') is-invalid @enderror"
                        id="phone" name="phone" value="{{ old('phone') }}"
                        value="{{ $user->phone }}" placeholder="No. Telepon">
                        @error('phone')
                            <label for="phone" class="invalid-feedback">{{ $message }}</label>
                        @enderror
                </div>
                <div class="form-group">
                    <label for="level">Level</label>
                    <select name="level" id="level" class="form-control">
                        <option value="user" {{ (old('level') == 'user' ? 'selected' : '') }}>User</option>
                        <option value="admin" {{ (old('level') == 'admin' ? 'selected' : '') }}>Administrator</option>
                    </select>
                        @error('email')
                            <label for="email" class="invalid-feedback">{{ $message }}</label>
                        @enderror
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-save"></i> Submit</button>
                    <a href="{{ route('category.index') }}" class="btn btn-danger btn-sm"><i class="fas fa-redo-alt"></i> Batal</a>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection

@push('script')
@endpush
