@extends('layouts.main')

@section('title', 'User')

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active">User</li>
@endsection

@section('content')

<div class="card">
    <div class="card-header">Daftar User</div>
    <div class="card-body">
        <div class="form-group">
            <a href="{{ route('user.create') }}" class="btn btn-success btn-sm">
                <i class="fas fa-plus-circle"></i>
                Tambah</a>
        </div>

        <div class="form-group">
            <table id="table_users" class="table table-striped">
                <thead>
                    <tr>
                        <th width="10%">No.</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>No. Telepon</th>
                        <th>level</th>
                        <th width="25%">Aksi</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    var table = $("#table_users").DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('user.index') }}',
        order: [[1, 'asc']],
        columns: [
            {data: 'DT_RowIndex', searchable: 'false', orderable: 'false'},
            {data: 'name'},
            {data: 'email'},
            {data: 'phone'},
            {data: 'level'},
            {data: 'action', searchable: 'false', orderable: 'false'}
        ],
        // columnDefs: [
        //     {
        //         orderable: true,
        //         className: 'reorder',
        //         targets: 1,
        //     },
        //     {
        //         orderable: false,
        //         targets: '_all'
        //     }
        // ]
        drawCallback: function() {
            confirmDelete();
        }

    });

</script>
@endpush
