@extends('layouts.main')

@section('title', 'Kategori Produk')

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active">Kategori Produk</li>
@endsection

@section('content')

{{-- @if (session('status'))
<div class="alert {{ session('status') == 'success' ? 'alert-success' : 'alert-danger' }}">
    {{ session('message') }}
</div>
@endif --}}

<div class="card">
    <div class="card-header">Daftar Kategori</div>
    <div class="card-body">
        <div class="form-group">
            <a href="{{ route('category.create') }}" class="btn btn-success btn-sm">
                <i class="fas fa-plus-circle"></i>
                Tambah</a>
        </div>

        <div class="table-responsive form-group">
            <table id="table_categories" class="table table-striped">
                <thead>
                    <tr>
                        <th width="10%">No.</th>
                        <th>Nama Kategori</th>
                        <th width="25%">Aksi</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    var table = $("#table_categories").DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('category.index') }}',
        order: [[1, 'asc']],
        columns: [
            {data: 'DT_RowIndex', searchable: 'false', orderable: 'false'},
            {data: 'category_name'},
            {data: 'action', searchable: 'false', orderable: 'false'}
        ],
        // columnDefs: [
        //     {
        //         orderable: true,
        //         className: 'reorder',
        //         targets: 1,
        //     },
        //     {
        //         orderable: false,
        //         targets: '_all'
        //     }
        // ]
        drawCallback: function() {
            confirmDelete();
        }

    });

</script>
@endpush
