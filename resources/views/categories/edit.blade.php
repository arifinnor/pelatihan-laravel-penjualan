@extends('layouts.main')

@section('title', 'Kategori Produk')

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('category.index') }}">Daftar Kategori</a></li>
<li class="breadcrumb-item active">Edit Kategori</li>
@endsection

@section('content')
<div class="card">
    <div class="card-header">Edit Kategori</div>
    <div class="card-body">
        <div class="container">
            <form action="{{ route('category.update', [$category->id]) }}" method="post">
                @csrf
                @method('put')

                <div class="form-group">
                    <label for="category_name">Nama Kategori</label>
                    <input type="text" class="form-control @error('category_name') is-invalid @enderror" id="category_name" name="category_name"
                        placeholder="Nama Kategori" value="{{ $category->category_name }}">
                        @error('category_name')
                            <label for="category_name" class="invalid-feedback">{{ $message }}</label>
                        @enderror
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                    <a href="{{ route('category.index') }}" class="btn btn-danger btn-sm">Batal</a>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection

@push('script')
@endpush
