@extends('layouts.main')

@section('title', 'Produk')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item active">Produk</li>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">Daftar Produk</div>
        <div class="card-body">
            <div class="form-group">
                <a href="{{ route('product.create') }}" class="btn btn-success btn-sm">
                    <i class="fas fa-plus-circle"></i>
                    Tambah</a>
            </div>

            <div class="table-responsive form-group">
                <table id="table_products" class="table table-striped" style="width: 100%">
                    <thead>
                        <tr>
                            <th width="10%">No.</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Kategori</th>
                            <th>Stok</th>
                            <th>Harga</th>
                            <th width="25%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        var table = $("#table_products").DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('product.index') }}',
            order: [
                [1, 'asc']
            ],
            columns: [{
                    data: 'DT_RowIndex',
                    searchable: 'false',
                    orderable: 'false'
                },
                {
                    data: 'product_code'
                },
                {
                    data: 'product_name'
                },
                {
                    data: 'category_id'
                },
                {
                    data: 'stock'
                },
                {
                    data: 'price'
                },
                {
                    data: 'action',
                    searchable: 'false',
                    orderable: 'false'
                }
            ],
            // columnDefs: [
            //     {
            //         orderable: true,
            //         className: 'reorder',
            //         targets: 1,
            //     },
            //     {
            //         orderable: false,
            //         targets: '_all'
            //     }
            // ]
            drawCallback: function() {
                confirmDelete();
            }

        });

    </script>
@endpush
