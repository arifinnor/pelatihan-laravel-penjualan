@extends('layouts.main')

@section('title', 'Produk')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('product.index') }}">Daftar Produk</a></li>
    <li class="breadcrumb-item active">Tambah Produk</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">Tambah Produk</div>
        <div class="card-body">
            <div class="container">
                <form action="{{ route('product.update', $product) }}" method="post">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for="product_code">Kode Produk</label>
                        <input type="text" class="form-control @error('product_code') is-invalid @enderror"
                            id="product_code" name="product_code"
                            value="{{ old('product_code') ?? $product->product_code }}" placeholder="Kode Produk">
                        @error('product_code')
                            <label for="product_code" class="invalid-feedback">{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="product_name">Nama Produk</label>
                        <input type="text" class="form-control @error('product_name') is-invalid @enderror"
                            id="product_name" name="product_name"
                            value="{{ old('product_name') ?? $product->product_name }}" placeholder="Nama Produk">
                        @error('product_name')
                            <label for="product_name" class="invalid-feedback">{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="caegory_id">Kategori</label>
                        <select name="category_id" id="category_id" class="form-control">
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}"
                                    {{ $category->id == $product->category_id ? 'selected' : '' }}>
                                    {{ $category->category_name }}</option>
                            @endforeach
                        </select>
                        @error('product_name')
                            <label for="product_name" class="invalid-feedback">{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="stock">Stok</label>
                        <input type="number" class="form-control @error('stock') is-invalid @enderror" id="stock"
                            name="stock" min=0 value="{{ old('stock') ?? $product->stock }}" placeholder="Jumlah Stok">
                        @error('stock')
                            <label for="stock" class="invalid-feedback">{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="price">Harga</label>
                        <input type="number" class="form-control @error('price') is-invalid @enderror" id="price"
                            name="price" min=0 value="{{ old('price') ?? $product->price }}" placeholder="Jumlah Stok">
                        @error('price')
                            <label for="price" class="invalid-feedback">{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        <a href="{{ route('product.index') }}" class="btn btn-danger btn-sm">Batal</a>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@push('script')
@endpush
