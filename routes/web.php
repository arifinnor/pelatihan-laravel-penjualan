<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes([
    'register' => false,
    'verify' => false,
    'reset' => false,
    'confirm' => false

]);

// level admin
Route::middleware(['auth', 'checkuser:admin'])->group(function () {
    Route::resource('user', UserController::class);
});

// level  all user
Route::middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return view('home');
    });

    Route::resources([
        'category' => CategoryController::class,
        'product' => ProductController::class,
        'customer' => CustomerController::class,
        'order' => OrderController::class

    ]);
});
