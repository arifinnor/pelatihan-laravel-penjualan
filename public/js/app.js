function sweetAlert(status, message) {
    Swal.fire({
        icon: status,
        title: 'Informasi',
        text: message,
        showConfirmButton: false,
        timer: 2000
    });
}

function confirmDelete() {
    $('a[data-confirm]').on('click', function (e) {
        e.preventDefault();

        Swal.fire({
            title: 'Peringatan',
            text: $(this).data('confirm'),
            icon: 'warning',
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return new Promise(resolve => {
                    setTimeout(() => {
                        resolve();
                    }, 500);
                });
            },
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Tidak'
        }).then(result => {
            if (result.value) {
                $.ajax({
                    type: 'delete',
                    url: $(this).attr('href'),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: response => {
                        //show alert
                        sweetAlert(response.status, response.message);
                        //reload datatable
                        table.ajax.reload();
                    }
                });
            }
        });
    });
}
