<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Fungsi untuk validasi form
     *
     * @param Request $request
     * @return void
     */
    public function validateForm($request, $id = null)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'password' => 'required|max:16',
                'email' => 'required|max:65|email|unique:users, email'

            ];
        } else {
            $rules = [
                'email' => 'required|max:65|email|unique:users, email,' . $id
            ];
        }

        $this->validate($request, [
            'name' => 'required|between:5,65',
            'phone' => 'required|max:15',
        ] + $rules);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $users = User::select('id', 'name', 'email', 'phone', 'level');

            return Datatables::of($users)
                ->addColumn('action', function ($user) {
                    return '
                        <a href="' . route('user.edit', [$user->id]) . '"
                            class="btn btn-sm btn-primary">
                            <i class="fas fa-edit"></i>
                            Edit
                        </a>

                        <a href="' . route('user.destroy', [$user->id]) . '"
                            class="btn btn-sm btn-danger"
                            data-confirm="Apakah anda yakin?">
                            <i class="fas fa-trash"></i>
                            Hapus
                        </a>
                    ';
                })
                ->addIndexColumn()
                ->toJson();
        }

        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            User::create($request->except('password') + [
                'password' => encrypt($request->input('password'))
            ]);

            $status = [
                'status' => 'success',
                'message' => 'Data berhasil disimpan'
            ];
        } catch (\Exception $e) {
            $status = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }

        return redirect('user')->with($status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit', compact($user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validateForm($request, $user->id);

        try {
            $user->update($request->all());

            $status = [
                'status' => 'success',
                'message' => 'Data berhasil diupdate'
            ];
        } catch (\Exception $e) {
            //exception $e;
            $status = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }
        return redirect('category')->with($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
