<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{

    /**
     * Fungsi untuk validasi form
     *
     * @param Request $request
     * @return void
     */
    public function validateForm($request)
    {
        $this->validate($request, [
            'category_name' => 'required|max:30'
        ], [
            'category_name.required' => 'Nama kategori tidak boleh kosong',
            'category_name.max' => 'Panjang maksimum 30 karakter'
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $categories = Category::select('id', 'category_name');

            return Datatables::of($categories)
                ->addColumn('action', function ($category) {
                    return '
                        <a href="' . route('category.edit', [$category->id]) . '"
                            class="btn btn-sm btn-primary">
                            <i class="fas fa-edit"></i>
                            Edit
                        </a>

                        <a href="' . route('category.destroy', [$category->id]) . '"
                            class="btn btn-sm btn-danger"
                            data-confirm="Apakah anda yakin?">
                            <i class="fas fa-trash"></i>
                            Hapus
                        </a>
                    ';
                })
                ->addIndexColumn()
                ->toJson();
        }

        return view('categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateForm($request);

        try {
            Category::create($request->all());

            $status = [
                'status' => 'success',
                'message' => 'Data berhasil disimpan'
            ];
        } catch (\Exception $e) {
            $status = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }

        return redirect('category')->with($status);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        dd($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->validateForm($request);
        try {
            $category->update($request->all());

            $status = [
                'status' => 'success',
                'message' => 'Data berhasil diupdate'
            ];
        } catch (\Exception $e) {
            //exception $e;
            $status = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }
        return redirect('category')->with($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $result = $category->delete();

        if ($result > 0) {
            $response = [
                'status' => 'success',
                'message' => 'Kategori berhasil dihapus'
            ];
        } else {
            $response = [
                'status' => 'success',
                'message' => 'Gagal menghapus kategori'
            ];
        }

        return response()->json($response);
    }
}
