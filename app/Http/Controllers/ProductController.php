<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $products = Product::with('category');

            return datatables()->of($products)
                ->addColumn('action', function ($product) {
                    return '
                        <a href="' . route('product.edit', [$product->id]) . '"
                            class="btn btn-sm btn-primary">
                            <i class="fas fa-edit"></i>
                            Edit
                        </a>

                        <a href="' . route('product.destroy', [$product->id]) . '"
                            class="btn btn-sm btn-danger"
                            data-confirm="Apakah anda yakin?">
                            <i class="fas fa-trash"></i>
                            Hapus
                        </a>
                    ';
                })
                ->editColumn('category_id', function ($product) {
                    return $product->category->category_name;
                })
                ->addIndexColumn()
                ->rawColumns(['action', 'category_id'])
                ->toJson();
        }

        return view('products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();
        return view('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ProductRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        try {
            Product::create($request->all());

            $status = [
                'status' => 'success',
                'message' => 'Data berhasil disimpan'
            ];
        } catch (\Exception $e) {
            $status = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }

        return redirect()->route('product.index')->with($status);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::get();
        return view('products.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProductRequest $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        try {
            $product->update($request->all());

            $status = [
                'status' => 'success',
                'message' => 'Data berhasil disimpan'
            ];
        } catch (\Exception $e) {
            $status = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }

        return redirect()->route('product.index')->with($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {
            $product->delete();

            $status = [
                'status' => 'success',
                'message' => 'Data berhasil dihapus'
            ];
        } catch (\Exception $e) {
            $status = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }

        return \response()->json($status);
    }
}
