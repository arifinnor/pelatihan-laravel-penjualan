<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required|exists:categories,id',
            // 'product_code' => ['required', 'max:5', Rule::unique('products', 'product_code')->ignore($this->product)],
            'product_code' => ['required', 'max:5', 'unique:products,product_code,' . $this->product->id],
            'product_name' => 'required|max:50',
            'stock' => 'required|numeric',
            'price' => 'required|numeric'
        ];
    }
}
